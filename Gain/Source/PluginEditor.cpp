/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#include "PluginProcessor.h"
#include "PluginEditor.h"


//==============================================================================
GainAudioProcessorEditor::GainAudioProcessorEditor (GainAudioProcessor& p)
    : AudioProcessorEditor (&p), processor (p)
{
    setSize (100, 300);

	addAndMakeVisible(gainSlider);
	gainSlider.setSliderStyle(Slider::LinearVertical);
	gainSlider.setTextBoxStyle(Slider::TextBoxBelow, false, 50, 30);
	gainSlider.setRange(0, 2, 0.01);
	gainSlider.setValue(1);
	gainSlider.addListener(this);

	addAndMakeVisible(gainLabel);
	gainLabel.setText("Gain", dontSendNotification);
	gainLabel.setJustificationType(Justification::centred);
}

GainAudioProcessorEditor::~GainAudioProcessorEditor()
{
}

//==============================================================================
void GainAudioProcessorEditor::sliderValueChanged(Slider* slider)
{
	if (slider == &gainSlider)
		processor.updateGain(gainSlider.getValue());
}
//==============================================================================
void GainAudioProcessorEditor::paint (Graphics& g)
{
    // (Our component is opaque, so we must completely fill the background with a solid colour)
    g.fillAll (Colours::darkseagreen);

	gainSlider.setColour(Slider::thumbColourId, Colours::rosybrown);
}

void GainAudioProcessorEditor::resized()
{
	const int labelWidth = getWidth();
	const int labelHeight = 30;
	const int sliderWidth = getWidth();
	const int sliderHeight = getHeight() - labelHeight;

	gainLabel.setBounds(0, sliderHeight, labelWidth, labelHeight);
	gainSlider.setBounds(0, 0, sliderWidth, sliderHeight);
}
