/*
  ==============================================================================

    This file was auto-generated!

    It contains the basic framework code for a JUCE plugin editor.

  ==============================================================================
*/

#pragma once

#include "../JuceLibraryCode/JuceHeader.h"
#include "PluginProcessor.h"


//==============================================================================
/**
*/
class GainAudioProcessorEditor  : public AudioProcessorEditor,
								  public SliderListener
{
public:
    GainAudioProcessorEditor (GainAudioProcessor&);
    ~GainAudioProcessorEditor();

    //==============================================================================
	void sliderValueChanged(Slider* slider) override;

	//==============================================================================
    void paint (Graphics&) override;
    void resized() override;

private:

	Slider gainSlider;
	Label gainLabel;

    // This reference is provided as a quick way for your editor to
    // access the processor object that created it.
    GainAudioProcessor& processor;

    JUCE_DECLARE_NON_COPYABLE_WITH_LEAK_DETECTOR (GainAudioProcessorEditor)
};
